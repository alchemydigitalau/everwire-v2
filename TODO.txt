Search articles based on proximity to place(s)
Articles based on category
Order articles based on Like count

Use Place getBounds to calculate area (sq km) - use as part of advertising pricing structure
...also calculate the radius needed for proximity searches? - warn that area is very big?

Use area boundaries in searches - subsearch of places that are within bounds e.g. Fortitude Valley will be within Brisbane, but probably > 10km from center

Advertising - anyone can set up advertising? - advertising is reportable?