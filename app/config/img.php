<?php

return array(
			'presets'	=> array(
								'l'	=> array(
											'width'		=> '1024',
											'height'	=> '1024',
											'type'		=> 'bounding'
											),
								'strip'	=> array(
											'width'		=> '695',
											'height'	=> '300',
											'type'		=> 'crop'
											),
								's'	=> array(
											'width'		=> '300',
											'height'	=> '300',
											'type'		=> 'crop'
											),
								't'	=> array(
											'width'		=> '80',
											'height'	=> '80',
											'type'		=> 'crop'
											),

								)
			);