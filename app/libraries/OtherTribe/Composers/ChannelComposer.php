<?php

namespace OtherTribe\Composers;

use DB, Auth;

class ChannelComposer
{
	public function compose($view)
	{
		if(Auth::check())
		{
			$subscriptions = DB::table('subscriptions')->whereUserId(Auth::user()->id)->lists('channel_id');
			
			$view->with('subscriptions', $subscriptions);
		}
	}
}