<?php

namespace OtherTribe\Composers;

use DB, Auth;

class PlaceComposer
{
	public function compose($view)
	{
		if(Auth::check())
		{
			$favouritePlaces = DB::table('place_user')->whereUserId(Auth::user()->id)->lists('place_id');

			$view->with('favouritePlaces', $favouritePlaces);
		}
	}
}