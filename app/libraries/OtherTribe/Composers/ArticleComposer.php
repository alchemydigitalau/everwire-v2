<?php

namespace OtherTribe\Composers;

use DB, Auth;

class ArticleComposer
{
	public function compose($view)
	{
		if(Auth::check())
		{
			$likes = DB::table('likes')->whereUserId(Auth::user()->id)->lists('article_id');
			$bookmarks = DB::table('bookmarks')->whereUserId(Auth::user()->id)->lists('article_id');

			$view->with('likes', $likes);
			$view->with('bookmarks', $bookmarks);
		}
	}
}