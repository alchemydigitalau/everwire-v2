<?php

namespace OtherTribe\Composers;

use DB, Auth;

class UserComposer
{
	public function compose($view)
	{
		if(Auth::check())
		{
			$followed = DB::table('followers')->whereFollowerId(Auth::user()->id)->lists('user_id');

			$view->with('followed', $followed);
		}
	}
}