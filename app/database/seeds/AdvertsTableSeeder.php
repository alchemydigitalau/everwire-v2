<?php

class AdvertsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker\Factory::create('en_GB');
                $faker->addProvider(new Faker\Provider\Lorem($faker));
                $faker->addProvider(new Faker\Provider\Company($faker));
                $faker->addProvider(new Faker\Provider\Internet($faker));
        	
                // 100
                for($n = 0; $n < 100; $n++)
                {
                	// Get a user
                	$user = User::orderBy(DB::raw('RAND()'))->first();

                	$advert = Advert::create(array(
        						'user_id'	=> $user->id,
        						'title'		=> $faker->catchPhrase(),
        						'subtitle'	=> $faker->sentence(rand(10, 20)),
        						'link'		=> $faker->url()
        						));

                	$places = Place::orderBy(DB::raw('RAND()'))->take(rand(1,5))->get();

                	foreach($places as $place)
                	{
                		$place->adverts()->attach($advert->id);
                	}
                }
	}

}