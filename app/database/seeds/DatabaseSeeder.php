<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PlacesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ChannelsTableSeeder');
		$this->call('ArticlesTableSeeder');
		$this->call('LikesTableSeeder');
		$this->call('BookmarksTableSeeder');
		$this->call('SubscribersTableSeeder');
		$this->call('AdvertsTableSeeder');
	}

}