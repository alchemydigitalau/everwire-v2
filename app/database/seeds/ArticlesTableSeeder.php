<?php

use \Michelf\Markdown;

class ArticlesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker\Factory::create('en_GB');
                $faker->addProvider(new Faker\Provider\Lorem($faker));
        	
                $filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/articles' ));

                // 300
                for($n = 0; $n < 300; $n++)
                {
                	$file = null;
                        
                        if(rand(0, 1))
                        {
                                try
                                {
                                        $file = file_get_contents('http://lorempixel.com/1024/768');
                                } catch(Exception $e)
                                {

                                }
                        }

                        // Get a user
                	$user = User::orderBy(DB::raw('RAND()'))->first();

                	$article = new Article;

                	$article->user_id 		= $user->id;
                	$article->place_id 		= $user->places()->first()->id;
                	$article->title 		= $faker->sentence(rand(3, 10));
                	$article->subtitle 		= $faker->sentence(rand(10, 20));
                        $article->preview               = ( ! empty($file)) ? md5( uniqid() ) . '.jpg' : null;
                	$article->published_at 	= $faker->dateTimeBetween('-4 months');

                	$payload = new Story;
                	$payload->body = implode("\n\n", $faker->paragraphs(rand(4, 12)));

                	$payload->save();
                	$payload->articles()->save($article);

                	if(rand(0,1))
                	{
                	       Channel::orderBy(DB::raw('RAND()'))->first()->articles()->attach($article->id);
                	}

                        if( ( ! empty($file)))
                        {
                                // pull in a random image...
                                $filesystem->write( $article->id . '/' . $article->preview, $file);
                        }
                }
	}

}
