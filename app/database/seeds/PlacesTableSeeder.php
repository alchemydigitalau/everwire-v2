<?php

class PlacesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		//DB::table('places')->truncate();

            $places = array(
                        'Brixton, United Kingdom',
                        'Clapham Junction, London Borough of Wandsworth, United Kingdom',
                        'Clapham Common, London Borough of Lambeth, United Kingdom',
                        'Fulham, United Kingdom',
                        'Putney Bridge, London, United Kingdom',
                        'Wimbledon, United Kingdom',
                        'South Kensington, United Kingdom',
                        'Victoria, City of Westminster, United Kingdom',
                        'London Borough of Southwark, United Kingdom',
                        'Bermondsey, London, United Kingdom',
                        'Farringdon, London Borough of Islington, United Kingdom',
                        'London Borough of Islington, United Kingdom',
                        'London Borough of Hackney, United Kingdom',
                        'Shoreditch, London, United Kingdom',
                        'Stratford, United Kingdom',
                        'London Borough of Tower Hamlets, United Kingdom',
                        'Morningside, Edinburgh, United Kingdom',
                        'Stockbridge, Edinburgh, United Kingdom',
                        'Portobello, United Kingdom',
                        'Leith, Scotland, United Kingdom',
                        'Newington, Edinburgh, United Kingdom',
                        'Murrayfield, Edinburgh, United Kingdom',
                        'Corstorphine, Edinburgh, United Kingdom',
                        'Liberton, Edinburgh, United Kingdom',
                        'Gilmerton, Edinburgh, United Kingdom',
                        'Livingston, United Kingdom',
                        'Musselburgh, United Kingdom',
                        'Dalkeith, United Kingdom',
                        'Penicuik, United Kingdom'
                        );

            /*$places = array(
                        
                        'Stockbridge, Edinburgh, United Kingdom',
                        'Portobello, United Kingdom',
                        'Leith, Scotland, United Kingdom',
                        'Newington, Edinburgh, United Kingdom',
                        'Murrayfield, Edinburgh, United Kingdom',
                        'Corstorphine, Edinburgh, United Kingdom',
                        'Liberton, Edinburgh, United Kingdom',
                        'Gilmerton, Edinburgh, United Kingdom',
                        'Livingston, United Kingdom',
                        'Musselburgh, United Kingdom',
                        'Dalkeith, United Kingdom',
                        'Penicuik, United Kingdom'
                        );*/

            foreach ($places as $place) {
                  Place::find_place($place);
            }
	}

}
