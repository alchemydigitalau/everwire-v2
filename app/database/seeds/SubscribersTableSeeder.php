<?php

class SubscribersTableSeeder extends Seeder {

	public function run()
	{
		// 100
		for($n = 0; $n < 100; $n++)
		{
			Channel::orderBy(DB::raw('RAND()'))->first()->subscribers()
					->attach(User::orderBy(DB::raw('RAND()'))->first());
		}
	}

}
