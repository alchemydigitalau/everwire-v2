<?php

class BookmarksTableSeeder extends Seeder {

	public function run()
	{
		// 50
		for($n = 0; $n < 50; $n++)
		{
			Article::orderBy(DB::raw('RAND()'))->first()->bookmarks()
					->attach(User::orderBy(DB::raw('RAND()'))->first());
		}
	}

}
