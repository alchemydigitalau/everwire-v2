<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker\Factory::create('en_GB');
		$faker->addProvider(new Faker\Provider\Internet($faker));

		$filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/users' ));

		$user = User::create(array(
	                                'first_name'  => 'Phil',
	                                'last_name'  => 'Stephens',
	                                'email' => 'phil@othertribe.com',
	                                //'avatar'	=> md5( uniqid() ) . '.jpg',
	                                'password'  => 'pcr34t366'
	                                ));

		//$filesystem->write( $user->id . '/' . $user->avatar, file_get_contents('https://m.ak.fbcdn.net/profile.ak/hprofile-ak-prn1/t1.0-1/p160x160/1524702_10202831637662761_1996256436_n.jpg') );

		foreach(array('Portobello', 'Stockbridge') as $pname)
		{
			Place::whereName($pname)->first()->users()->attach($user->id);
		}

		$user = User::create(array(
	                                'first_name'  => 'Colin',
	                                'last_name'  => 'Livingstone',
	                                'email' => 'colin@everwire.net',
	                                //'avatar'	=> md5( uniqid() ) . '.jpg',
	                                'password'  => 'paris'
	                                ));

		//$filesystem->write( $user->id . '/' . $user->avatar, file_get_contents('https://m.ak.fbcdn.net/profile.ak/hprofile-ak-prn2/t1.0-1/c0.0.160.160/p160x160/1511282_1423955757840790_274523658_n.jpg') );

		foreach(array('Leith', 'Stockbridge') as $pname)
		{
			Place::whereName($pname)->first()->users()->attach($user->id);
		}

		// 100
		for ($n = 0; $n < 100; $n++)
        {
			$file = null;
			
			try
			{
				$file = file_get_contents('http://lorempixel.com/768/1024/people');
			} catch(Exception $e)
			{

			}

			$user = User::create(array(
	                                'first_name'  => $faker->firstName,
	                                'last_name'  => $faker->lastName,
	                                'email' => $faker->email,
	                                'avatar'	=> ( ! empty($file)) ? md5( uniqid() ) . '.jpg' : null,
	                                'password'  => str_random(8)
	                                ));
			
			if( ! empty($file))
			{
				$filesystem->write( $user->id . '/' . $user->avatar, $file);
			}

			Place::orderBy(DB::raw('RAND()'))->first()->users()->attach($user->id);
		}
	}

}
