<?php

class LikesTableSeeder extends Seeder {

	public function run()
	{
		// 1000
		for($n = 0; $n < 1000; $n++)
		{
			Article::orderBy(DB::raw('RAND()'))->first()->likes()
					->attach(User::orderBy(DB::raw('RAND()'))->first());
		}
	}

}
