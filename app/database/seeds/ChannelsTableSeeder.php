<?php

class ChannelsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker\Factory::create('en_GB');

                $faker->addProvider(new Faker\Provider\Company($faker));
                $faker->addProvider(new Faker\Provider\Lorem($faker));

                $filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/channels' ));

                // 40
                for($n = 0; $n < 40; $n++)
                {
                	$file = null;
                        
                        try
                        {
                                $file = file_get_contents('http://lorempixel.com/1024/768');
                        } catch(Exception $e)
                        {

                        }

                        $channel = Channel::create(array(
                	                                'user_id'  	=> User::orderBy(DB::raw('RAND()'))->first()->id,
                	                                'name'      	=> $faker->catchPhrase(),
                	                                'description'	=> $faker->sentence(rand(10, 20)),
                                                        'preview'       => ( ! empty($file)) ? md5( uniqid() ) . '.jpg' : null
                	                                ));

                        if( ! empty($file))
                        {
                                $filesystem->write( $channel->id . '/' . $channel->preview, $file);
                        }
                        

                	//$channel->users()->attach($channel->user_id);
                	$channel->subscribers()->attach($channel->user_id);

                	$users = User::orderBy(DB::raw('RAND()'))->limit(4)->where('id', '!=', $channel->user_id)->get();

                	foreach($users as $user)
                	{
                		$channel->subscribers()->attach($user->id);
                	}
                }
	}

}
