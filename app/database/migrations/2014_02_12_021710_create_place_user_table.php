<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlaceUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('place_user', function(Blueprint $table) {
			$table->integer('place_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('sensitivity')->default(10)->unsigned();
			$table->string('name')->nullable()->default(null);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('place_user');
	}

}
