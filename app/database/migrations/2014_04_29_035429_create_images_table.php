<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('gallery_id')->unsigned();
			$table->string('file_name')->nullable()->default(null);
			$table->string('original_name')->nullable()->default(null); // May be unnecessary as masking behind the preview file name on snap
			$table->string('file_type')->nullable()->default(null);
			$table->string('caption')->nullable()->default(null);
			$table->double('size')->nullable()->default(null);

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('images', function(Blueprint $table)
		{
			$table->dropForeign('images_user_id_foreign');
		});

		Schema::drop('images');
	}

}
