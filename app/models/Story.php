<?php

use \Michelf\Markdown;

class Story extends OtherTribe\Models\Eloquent
{
	protected $guarded = array('id');
    protected $softDelete = true;
    protected $before_save = array('processBody');
   
    public function articles()
    {
        return $this->morphMany('Article', 'payload');
    }

    public function processBody()
    {
        if( isset($this->attributes['body']))
        {
            $this->attributes['body_html'] = Markdown::defaultTransform($this->attributes['body']);
        }
    }

}