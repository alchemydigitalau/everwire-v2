<?php

class Place extends OtherTribe\Models\Eloquent {
	protected $guarded = array('id');
    public $timestamps = false;
    protected $before_save = array('set_name', 'set_area');

    /**
	* Relationships
	*/
    public function articles()
    {
        return $this->hasMany('Article');
    }

    public function adverts()
    {
        return $this->belongsToMany('Advert');
    }

    public function users()
    {
        return $this->belongsToMany('User');
    }

    /**
	* Methods
	*/
	public function set_name()
	{
		if( ! empty($this->attributes['full_name']))
        {	
          	$name = explode(',', $this->attributes['full_name']);

          	$this->attributes['name'] = trim(array_shift($name));

        }
	}

    public function set_area()
    {
        $this->attributes['bounds_area'] = $this->area();
    }

    public function set_preview()
    {
        if(empty($this->attributes['preview']))
        {   
            $this->attributes['preview'] = md5( uniqid() ) . '.png';
            $this->save();

            $query = array(
                            'center'    => $this->attributes['latitude'] . ',' . $this->attributes['longitude'],
                            'zoom'      => 15,
                            'sensor'    => 'false',
                            'size'      => '600x400',
                            'scale'     => 2,
                            'key'       => Config::get('geo.google_api_key')
                            );

            $image = Img::make( Config::get('geo.google_staticmaps') . '?' . http_build_query($query) )->greyscale();

            $image->encode('png');

            $filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/places' ));
            $filesystem->write( $this->attributes['preview'], $image );

           
        }
    }

    static function find_place($full_name)
    {
        // Does the place exist?
        if($place = self::whereFullName($full_name)->first())
        {
            return $place;
        }

        // Nope! Let's geocode and save
        try
        {
            $geocoded = self::geocode($full_name);
        
            // Does this place already exist - only under a slightly different name?
            // Try matching the exact lat/long coordinates
            if($place = self::whereLatitude( $geocoded->getLatitude() )->whereLongitude( $geocoded->getLongitude() )->first() )
            {
                return $place;
            }

            $place = array(
                        'full_name'     => $full_name,
                        'latitude'      => $geocoded->getLatitude(),
                        'longitude'     => $geocoded->getLongitude()
                        );

            $bounds = $geocoded->getBounds();

            if( ! empty($bounds))
            {
                foreach($bounds as $key => $val)
                {
                    $place['bounds_' . $key] = $val;
                }
            }

            $p = self::create($place);

            // Hand this off to a worker
            $p->set_preview();

            return $p;
        } catch(Exception $e)
        {
            return false;
        }
    }

    private static function geocode($full_name)
    {
        $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
        $chain = new \Geocoder\Provider\ChainProvider(
                                                        array(
                                                            new \Geocoder\Provider\GoogleMapsProvider($adapter),
                                                        )
                                                    );

        $geocoder = new \Geocoder\Geocoder();

        $geocoder->registerProvider($chain);

        return $geocoder->geocode($full_name);
    }

    private function area()
    {
        if(empty($this->attributes['bounds_north']))
        {
            return 1;
        }

        $ns = $this->distance(
                            $this->attributes['bounds_north'],
                            $this->attributes['bounds_west'],
                            $this->attributes['bounds_south'],
                            $this->attributes['bounds_west']
                            );

        $ew = $this->distance(
                            $this->attributes['bounds_north'],
                            $this->attributes['bounds_west'],
                            $this->attributes['bounds_north'],
                            $this->attributes['bounds_east']
                            );

        return (($ns * $ew) < 1) ? 1 : $ns * $ew;
    }

    private function distance($lat1, $lon1, $lat2, $lon2) 
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
 
        return ($miles * 1.609344);
    }

    public function preview($derivative = 'split')
    {
        $parts = array(
                        'img',
                        'cache',
                        $derivative,
                        'places',
                        $this->attributes['preview']
                        );

        return '/' . implode('/', $parts);
    }

}
