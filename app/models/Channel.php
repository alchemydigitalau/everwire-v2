<?php

class Channel extends OtherTribe\Models\Eloquent {
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function articles()
    {
        return $this->belongsToMany('Article');
    }

    public function users()
    {
        return $this->belongsToMany('User');
    }

    public function subscribers()
    {
    	return $this->belongsToMany('User', 'subscriptions');
    }

    public function preview($derivative = 'split')
    {
        $parts = array(
                        'img',
                        'cache',
                        $derivative,
                        'channels',
                        $this->attributes['id'],
                        $this->attributes['preview']
                        );

        return '/' . implode('/', $parts);
    }
}
