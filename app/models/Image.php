<?php

class Image extends OtherTribe\Models\Eloquent
{
	protected $guarded = array('id');
    protected $softDelete = true;
   
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function gallery()
    {
        return $this->belongsTo('Gallery');
    }

}