<?php

class Gallery extends OtherTribe\Models\Eloquent
{
	protected $guarded = array('id');
    protected $softDelete = true;
   
    public function articles()
    {
        return $this->morphMany('Article', 'payload');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }

}