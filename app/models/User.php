<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends OtherTribe\Models\Eloquent implements UserInterface, RemindableInterface {

	protected $softDelete = true;
	protected $guarded = array('id');

	protected $before_save = array('separate_name', 'hash_password');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
	

	/**
	* Relationships
	*/
	public function profiles()
    {
        return $this->hasOne('Profile');
    }

	public function channels()
    {
        return $this->hasMany('Channel');
    }

    public function articles()
    {
        return $this->hasMany('Article');
    }

    public function likes()
    {
        return $this->belongsToMany('Article', 'likes');
    }

    public function bookmarks()
    {
        return $this->belongsToMany('Article', 'bookmarks');
    }

    public function places()
    {
        return $this->belongsToMany('Place');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('Channel', 'subscriptions');
    }

    public function adverts()
    {
        return $this->hasMany('Advert');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }

    public function followers()
    {
    	return $this->belongsToMany('User', 'followers', 'user_id', 'follower_id');
    }

    public function following()
    {
    	return $this->belongsToMany('User', 'followers', 'follower_id', 'user_id');
    }
    
	/**
	* Methods
	*/

	public function hash_password()
	{
		if(isset($this->attributes['password']))
		{
			if($this->attributes['password'] != $this->getOriginal('password'))
			{
				$this->attributes['password'] = Hash::make($this->attributes['password']);
			}
		}
	}

	public function separate_name()
	{
		if( ! empty($this->attributes['name']))
        {	
          	$name = explode(' ', $this->attributes['name']);

          	$this->attributes['first_name'] = array_shift($name);
			$this->attributes['last_name'] = implode(' ', $name);

			unset($this->attributes['name']);
        }
	}

	public function avatar($derivative = 't')
    {
        $parts = array(
                        'img',
                        'cache',
                        $derivative
                        );

        if( ! empty($this->attributes['avatar']))
        {  
            $parts[] = 'users';
            $parts[] = $this->attributes['id'];
            $parts[] = $this->attributes['avatar'];
        } else
        {
            $parts[] = 'defaults';
            $parts[] = 'avatar.jpg';
        }

        return '/' . implode('/', $parts);
    }

}