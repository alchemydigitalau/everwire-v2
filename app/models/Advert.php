<?php

class Advert extends OtherTribe\Models\Eloquent {
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
    * Relationships
    */
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function place()
    {
        return $this->belongsToMany('Place');
    }

    /**
    * Other Methods
    */
    public static function insertInto($collection, $item_template, $advert_template)
    {
        $output = '';
        $a = mt_rand(ceil(count($collection) / 4), count($collection));
        $i = 1; 

        foreach($collection as $item)
        {
            $view = View::make($item_template, array('item' => $item));

            $output .= $view->render();

            if($i == $a)
            {
                $advert = Advert::getAdvert($item->place);



                $view = View::make($advert_template, compact('advert'));

                $output .= $view->render();
            }

            $i++;
        }

        return $output;
    }

    public static function getAdvert($place, $sensitivity = 5)
    {
    
        // Retrieve an advert based on the place - or nearby...
        $advert = Advert::select('adverts.*')
                        
                        ->groupBy('adverts.id')
                        ->orderBy(DB::raw('RAND()'))

                        ->join('advert_place', 'adverts.id', '=', 'advert_place.advert_id')
                        ->join('places', 'advert_place.place_id', '=', 'places.id')
                        // Where place position is with $sensitivity km of $place
                        ->where(DB::raw('(6371 * 
                                        ACOS(SIN(RADIANS(' . $place->latitude . ')) * 
                                        SIN(RADIANS(places.latitude)) + 
                                        COS(RADIANS(' . $place->latitude . ')) * 
                                        COS(RADIANS(places.latitude)) * 
                                        COS(RADIANS(' . $place->longitude . ') - 
                                        RADIANS(places.longitude))))'), '<', $sensitivity);

        if( ! empty($place->bounds_north))
        {
            // Or where place position is within the bounds of $place
            $advert->orWhere(function($query) use ($place)
                    {
                        $query->where('places.latitude', '>', $place->bounds_south)
                              ->where('places.latitude', '<', $place->bounds_north)
                              ->where('places.longitude', '>', $place->bounds_west)
                              ->where('places.longitude', '<', $place->bounds_east);
                    });
        }

        $advert->take(1)->get();

        // @todo If it's empty, generate an internal ad
        if($advert->count())
        {
            return $advert->first();
        }

        $advert = new Advert;
        $advert->title = 'Advertise in ' . $place->name;
        $advert->subtitle = "It's cheaper than you think!";
        $advert->link = 'http://everwire.net/ads';

        return $advert;
    }
}
