<?php

class Article extends OtherTribe\Models\Eloquent {
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function channel()
    {
        return $this->belongsToMany('Channel');
    }

    public function place()
    {
        return $this->belongsTo('Place');
    }

    public function likes()
    {
        return $this->belongsToMany('User', 'likes');
    }

    public function bookmarks()
    {
        return $this->belongsToMany('User', 'bookmarks');
    }

    public function payload()
    {
        return $this->morphTo();
    }

    /**
	* Other Methods
	*/
    public static function getSubscription(User $user)
    {
        $byChannel = Article::select('articles.id')
                        ->groupBy('articles.id')
                        ->join('article_channel', 'articles.id', '=', 'article_channel.article_id')
                        ->join('channels', 'article_channel.channel_id', '=', 'channels.id')
                        ->join('subscriptions', 'subscriptions.channel_id', '=', 'channels.id')                        
                        ->where('subscriptions.user_id', $user->id)
                        ->whereNotNull('articles.published_at')
                        //->remember(10)
                        ->lists('articles.id');

        $byUser = Article::select('articles.id')
                        ->groupBy('articles.id')
                        ->join('followers', 'articles.user_id', '=', 'followers.user_id')
                        ->where('followers.follower_id', $user->id)
                        ->whereNotNull('articles.published_at')
                        ->lists('articles.id');

        // Need to enhance this for proximity
        $places = $user->places;

        // Seed with 0 so that the return value has _something_ to search on
        $byPlace = array(0);

        foreach($places as $place)
        {
            $byPlace = array_merge(
                                    Article::byPlace($place)->lists('articles.id'), 
                                    $byPlace
                                    );
        }
        
        // Query the group results
        return Article::whereIn('id', array_merge($byChannel, $byUser, $byPlace));
    }

    public static function byPlace(Place $place, $sensitivity = 5)
    {
        // this where magical shit will happen!
        $articles = Article::select('articles.*')
                        ->groupBy('articles.id')
                        ->join('places', 'articles.place_id', '=', 'places.id')
                        // Where place position is with $sensitivity km of $place
                        ->whereNotNull('articles.published_at')
                        ->where(DB::raw('(6371 * 
                                        ACOS(SIN(RADIANS(' . $place->latitude . ')) * 
                                        SIN(RADIANS(places.latitude)) + 
                                        COS(RADIANS(' . $place->latitude . ')) * 
                                        COS(RADIANS(places.latitude)) * 
                                        COS(RADIANS(' . $place->longitude . ') - 
                                        RADIANS(places.longitude))))'), '<', $sensitivity);

        if( ! empty($place->bounds_north))
        {
            // Or where place position is within the bounds of $place
            $articles->orWhere(function($query) use ($place)
                    {
                        $query->where('places.latitude', '>', $place->bounds_south)
                              ->where('places.latitude', '<', $place->bounds_north)
                              ->where('places.longitude', '>', $place->bounds_west)
                              ->where('places.longitude', '<', $place->bounds_east)
                              ->whereNotNull('articles.published_at');
                    });
        }

        return $articles;
                        
    }

    public function preview($derivative = 'strip')
    {
        $parts = array(
                        'img',
                        'cache',
                        $derivative
                        );

        if( ! empty($this->attributes['preview']))
        {  
            $parts[] = 'articles';
            $parts[] = $this->attributes['id'];
            $parts[] = $this->attributes['preview'];
        } else
        {
            $parts[] = 'places';
            $parts[] = $this->place->preview;
        }

        return '/' . implode('/', $parts);
    }

    public function getDates()
	{
	    return array('created_at', 'updated_at', 'published_at');
	}
}
