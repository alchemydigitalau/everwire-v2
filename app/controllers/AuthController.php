<?php

class AuthController extends BaseController {

	public function register()
	{
		return View::make('auth/register');
	}

	public function handleRegister()
	{
		$validator = Validator::make(Input::all(), array(
										                'name'		=> 'required',
										                'email'		=> 'required|email|unique:users',
										                'password' 	=> 'required|min:6'
										            	));
	    if ($validator->passes())
	    {
	    	
	    	$user = User::create(Input::all());

	    	Auth::login($user);

	        return Redirect::action('UserController@settings');
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('auth/register', compact('messages'));
	}

	public function login()
	{
		return View::make('auth/login');
	}

	public function handleLogin()
	{
		$validator = Validator::make(Input::all(), array(
										                'email'		=> 'required|email',
										                'password' 	=> 'required'
										            	));
	    if ($validator->passes())
	    {
	        $credentials = array(
			                    'email' 	=> Input::get('email'),
			                    'password' 	=> Input::get('password')
	                			);
            
            if (Auth::attempt($credentials))
            {
                return Redirect::intended('/');
            } else
            {
            	$messages = new MessageBag(array(
	                							'password' => array(
		                    										'That email/password combination is invalid.'
		                											)
	            								));
            }
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('auth/login', compact('messages'));
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::action('HomeController@index');
	}

}
