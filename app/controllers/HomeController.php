<?php

class HomeController extends BaseController {

	public function index()
	{
		$orderBy = 'published_at';
		$sort = 'timeline';
		$place = '';

		// 20
		$paginate = 10;

		if(Input::has('sort') && Input::get('sort') == 'popular')
		{
			$orderBy = DB::raw('(SELECT count(likes.user_id) from likes where likes.article_id = articles.id)');
			$sort = 'popular';
		}

		if(Input::has('place'))
		{
			$place = Place::find_place(Input::get('place'));

			return Redirect::to('place/' . $place->id . '?sort=' . $sort);

		} elseif(Auth::check())
		{
			$articles = Article::getSubscription(Auth::user())
									->orderBy($orderBy, 'desc')
									->paginate($paginate);
		} else
		{
			$articles = Article::whereNotNull('published_at')->orderBy($orderBy, 'desc')->paginate($paginate);
		}

		// Set the map...
		if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
		{
			$adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
	        $chain = new \Geocoder\Provider\ChainProvider(
	                                                        array(
	                                                            new \Geocoder\Provider\FreeGeoIpProvider($adapter),
	                                                        )
	                                                    );

	        $geocoder = new \Geocoder\Geocoder();

	        $geocoder->registerProvider($chain);

	        $geocoded = $geocoder->geocode($_SERVER['REMOTE_ADDR']);

	        $location_str = $geocoded->getLatitude() . ', ' . $geocoded->getLongitude();

	        // We'll add some session caching here
		} else
		{
			$location_str = '55.9532520000, -3.1882670000';
		}

		return View::make('home/index', compact('articles', 'sort', 'place', 'location_str'));
	}

}