<?php

class ChannelController extends BaseController {

	public function index()
	{
		$orderBy = 'created_at';
		$sort = 'timeline';

		if(Input::has('sort') && Input::get('sort') == 'popular')
		{
			$orderBy = DB::raw('(SELECT count(subscriptions.user_id) from subscriptions where subscriptions.channel_id = channels.id)');
			$sort = 'popular';
		}

		$channels = Channel::orderBy($orderBy, 'desc')
							->paginate(12);

		return View::make('channel/index', compact('channels', 'sort'));
	}

	public function articles(Channel $channel)
	{
		$orderBy = 'published_at';
		$sort = 'timeline';

		if(Input::has('sort') && Input::get('sort') == 'popular')
		{
			$orderBy = DB::raw('(SELECT count(likes.user_id) from likes where likes.article_id = articles.id)');
			$sort = 'popular';
		}

		$articles = $channel->articles()
							->orderBy($orderBy, 'desc')
							->paginate(10);

		return View::make('channel/articles', compact('channel', 'articles', 'sort'));
	}

	public function user()
	{
		return View::make('channel/user');
	}

	public function create()
	{
		return View::make('channel/create');
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'name'			=> 'required',
														'description'	=> 'required'
										            	));

		if($validator->passes())
		{
        	$channel = new Channel;
        	$channel->user_id 		= Auth::user()->id;
        	$channel->name 			= Input::get('name');
        	$channel->description 	= Input::get('description');
        	
        	$channel->save();

        	return Redirect::action('ChannelController@user');
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('channel/create', compact('messages'));
	}

	public function edit(Channel $channel)
	{
		return View::make('channel/edit', compact('channel'));
	}

	public function handleEdit(Channel $channel)
	{
		$validator = Validator::make(Input::all(), array(
														'name'			=> 'required',
														'description'	=> 'required'
										            	));

		if($validator->passes())
		{
        	$channel->name 			= Input::get('name');
        	$channel->description 	= Input::get('description');
        	
        	$channel->save();

        	return Redirect::action('ChannelController@user');
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('channel/edit', compact('channel', 'messages'));
	}

	public function subscribe()
	{
		Auth::user()->subscriptions()->attach(Input::get('channel-id'));

		return Redirect::back();
	}

	public function unSubscribe(Channel $channel)
	{
		Auth::user()->subscriptions()->detach($channel->id);

		return Redirect::back();
	}

}
