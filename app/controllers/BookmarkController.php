<?php

class BookmarkController extends BaseController {

	public function index()
	{
		$articles = Auth::user()->bookmarks;

        return View::make('bookmark/index', compact('articles'));
	}

}
