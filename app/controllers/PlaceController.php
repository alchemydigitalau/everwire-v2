<?php

class PlaceController extends BaseController {

	public function articles(Place $place)
	{
		$orderBy = 'published_at';
		$sort = 'timeline';

		$paginate = 20;

		if(Input::has('sort') && Input::get('sort') == 'popular')
		{
			$orderBy = DB::raw('(SELECT count(likes.user_id) from likes where likes.article_id = articles.id)');
			$sort = 'popular';
		}

		$articles = Article::byPlace($place)
						->orderBy($orderBy, 'desc')
						->paginate($paginate);

		return View::make('place/articles', compact('place', 'articles', 'sort'));
	}


	public function favourite()
	{
		Auth::user()->places()->attach(Input::get('place-id'));

		return Redirect::back();
	}

	public function unFavourite(Place $place)
	{
		Auth::user()->places()->detach($place->id);

		return Redirect::back();
	}
}
