<?php

class ImageController extends BaseController {

	
	public function cache($args)
	{
		$parts = explode('/', $args);

		// Get the derivative
		$derivative = array_shift($parts);

		// Load the presets
		$presets = Config::get('img.presets');

		// If there's no size preset then there's little sense in carrying on...
		if(empty($presets[$derivative]))
		{
			die; // will replace with 404
		}

		$filename = array_pop($parts);

		$path = implode("/", $parts);


		$filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/' . $path ));

		if( ! $filesystem->has( $filename ))
		{
			die; // will replace with 404
		}

		try
		{
			// Load the preview image
			$image = Img::make( $filesystem->read( $filename ) );

			// Do the maniplation
			if($presets[$derivative]['type'] == 'bounding')
			{
				$image->resize($presets[$derivative]['width'], $presets[$derivative]['height'], TRUE);
			} else
			{
				$image->grab($presets[$derivative]['width'], $presets[$derivative]['height']);
			}

			// Assemble the destination path
			$destination_path = public_path() . '/img/cache/' . $derivative . '/' . $path;

			// create the destination directory structure if required
			if ( ! file_exists($destination_path)) 
			{
			    mkdir($destination_path, 0777, true);
			}

			$image->save($destination_path . '/' . $filename);

			return $image->response();
		} catch(Exception $e)
		{
			die; // will replace with 404
		}

		exit; // will replace with 404
	}

}
