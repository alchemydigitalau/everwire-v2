<?php

class UserController extends BaseController {

	public function settings()
	{
		return View::make('user/settings');
	}

	public function follow()
	{
		Auth::user()->following()->attach(Input::get('user-id'));

		return Redirect::back();
	}

	public function unFollow(User $user)
	{
		Auth::user()->following()->detach($user->id);

		return Redirect::back();
	}
}
