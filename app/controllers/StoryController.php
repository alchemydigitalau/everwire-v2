<?php

class StoryController extends BaseController {

	public function create()
	{
		return View::make('story/create');
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'subtitle'	=> 'required',
														'body'		=> 'required',
														'place'		=> 'required'
										            	));

		if($validator->passes())
		{
			$article = new Article;

        	$article->user_id 		= Auth::user()->id;
        	$article->title 		= Input::get('title');
        	$article->subtitle 		= Input::get('subtitle');
        	
        	if(Input::has('publish'))
        	{
        		$article->published_at 	= Carbon\Carbon::now()->toDateTimeString();
        	}

        	// Place
        	$article->place_id 		= Place::find_place(Input::get('place'))->id;

        	// Story payload
        	$payload = new Story;
        	$payload->body = Input::get('body');

        	$payload->save();
        	$payload->articles()->save($article);

        	return Redirect::action('ArticleController@show', $article->id);
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('story/create', compact('messages'));
	}

	public function edit(Article $article)
	{
		return View::make('story/edit', compact('article'));
	}

	public function handleEdit(Article $article)
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'subtitle'	=> 'required',
														'body'		=> 'required',
														'place'		=> 'required'
										            	));

		if($validator->passes())
		{
        	$article->title 		= Input::get('title');
        	$article->subtitle 		= Input::get('subtitle');
        	
        	if(Input::has('publish'))
        	{
        		$article->published_at 	= Carbon\Carbon::now()->toDateTimeString();
        	} else
        	{
        		$article->published_at = NULL;
        	}

        	// Place
        	$article->place_id 		= Place::find_place(Input::get('place'))->id;

        	// Story payload
        	$article->payload->body = Input::get('body');
        	$article->payload->save();

        	$article->save();

        	return Redirect::action('ArticleController@show', $article->id);
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('story/edit', compact('article', 'messages'));
	}


}
