<?php

class ArticleController extends BaseController {

	public function show(Article $article)
	{
		if(empty($article->published_at))
		{
			return Redirect::action('ArticleController@preview', $article->id);
		}

		$advert = View::make('partials/advert_grid', array('advert' => Advert::getAdvert($article->place) ));
		$advert->render();

		$user = $article->user;

		$by_user = Article::orderBy(DB::raw('RAND()'))->limit(2)->whereUserId($article->user_id)->where('id', '!=', $article->id)->whereNotNull('published_at')->get();
		$by_place = Article::orderBy(DB::raw('RAND()'))->limit(2)->wherePlaceId($article->place_id)->where('id', '!=', $article->id)->whereNotNull('published_at')->get();

		return View::make('article/show', compact('article', 'user', 'advert', 'by_user', 'by_place'));
	}

	public function preview(Article $article)
	{
		if($article->user->id != Auth::user()->id)
		{
			App::abort(403, 'Unauthorized.');
		}

		$advert = View::make('partials/advert_grid', array('advert' => Advert::getAdvert($article->place) ));
		$advert->render();

		return View::make('article/show', compact('article', 'advert'));
	}

	public function create()
	{
		return View::make('article/create');
	}

	public function edit(Article $article)
	{
		switch($article->payload_type)
		{
			case 'Story':
				return Redirect::action('StoryController@edit', $article->id);
				break;
		}
	}

	public function user(User $user)
	{
		$orderBy = 'published_at';
		$sort = 'timeline';

		if(Input::has('sort') && Input::get('sort') == 'popular')
		{
			$orderBy = DB::raw('(SELECT count(likes.user_id) from likes where likes.article_id = articles.id)');
			$sort = 'popular';
		}

		$articles = $user->articles()->whereNotNull('published_at')->orderBy($orderBy, 'desc')->paginate(10);

		return View::make('article/user', compact('articles', 'user', 'sort'));
	}

	public function drafts()
	{
		$sort = 'draft';
		$user = Auth::user();
		$articles = Auth::user()->articles()->wherePublishedAt(NULL)->paginate(10);

		return View::make('article/user', compact('articles', 'user', 'sort'));
	}


	public function like()
	{
		Auth::user()->likes()->attach(Input::get('article-id'));

		return Redirect::back();
	}

	public function unLike(Article $article)
	{
		Auth::user()->likes()->detach($article->id);

		return Redirect::back();
	}

	public function bookmark()
	{
		Auth::user()->bookmarks()->attach(Input::get('article-id'));

		return Redirect::back();
	}

	public function unBookmark(Article $article)
	{
		Auth::user()->bookmarks()->detach($article->id);

		return Redirect::back();
	}

}
