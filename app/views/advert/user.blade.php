@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1>Your Adverts <a href="{{ action('AdvertController@create') }}" class="btn btn-default"><i class="fa fa-plus"></i> New Advert</a></h1>

	<hr />
	
	<div class="row" id="container">
		@foreach(Auth::user()->adverts->all() as $advert)
			@include('partials/advert_grid')
		@endforeach
	</div>

</div>

@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

	<script type="text/javascript">
		<!--
		$(function() {

			var $container = $('#container');
			// initialize
			$container.masonry({
			  itemSelector: '.item'
			});

		});
		-->
	</script>

@stop