@extends('layouts/default')

@section('content')
<header class="masthead" style="background-image: url({{ $place->preview('l')}});">
	<div class="container">	
		<div class="masthead-title">
			<h1>Latest Articles in {{ $place->name }}<br /><small>{{ $place->full_name }}</small></h1>

			@include('partials/favourite-place-button')
		</div>
	</div>
</header>

<div class="container">
	<ul class="nav nav-tabs">
		<li{{ ($sort == 'timeline') ? ' class="active"' : '' }}><a href="{{ action('PlaceController@articles', $place->id) }}?sort=timeline">Most Recent</a></li>
		<li{{ ($sort == 'popular') ? ' class="active"' : '' }}><a href="{{ action('PlaceController@articles', $place->id) }}?sort=popular">Most Popular</a></li>
	</ul>

	<hr />

	<div class="row" id="container">
		{{ Advert::insertInto($articles, 'partials/article_grid', 'partials/advert_grid') }}
	</div>

	<div class="text-center">
		{{ $articles->appends(array('sort' => $sort))->links(); }}
	</div>
</div>

@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

	<script type="text/javascript">
		<!--
		$(function() {

			var $container = $('#container');
			// initialize
			$container.masonry({
			  itemSelector: '.item'
			});

		});
		-->
	</script>

@stop