@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1>Your Bookmarks</h1>

	<hr />

	<div class="row" id="container">
		{{ Advert::insertInto($articles, 'partials/article_grid', 'partials/advert_grid') }}	
	</div>

</div>

@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

	<script type="text/javascript">
		<!--
		$(function() {

			var $container = $('#container');
			// initialize
			$container.masonry({
			  itemSelector: '.item'
			});

		});
		-->
	</script>

@stop