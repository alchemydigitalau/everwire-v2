@extends('layouts/default')

@section('content')
<div class="container">
	<form role="form">
		<input type="text" class="form-control" id="pac-input" name="place" placeholder="Where are you?">

		<button type="submit" class="btn btn-default">Search</button>
	</form>
</div>

<div class="container">
	<div class="row">

		<div class="col-md-9">
			<h1>Latest Articles<br /><small>{{ $place }}</small></h1>

			<ul class="nav nav-tabs">
				<li {{ ($sort == 'timeline') ? ' class="active"' : '' }}><a href="{{ action('HomeController@index') }}?sort=timeline&place={{ $place }}">Most Recent</a></li>
				<li {{ ($sort == 'popular') ? ' class="active"' : '' }}><a href="{{ action('HomeController@index') }}?sort=popular&place={{ $place }}">Most Popular</a></li>
			</ul>

			@foreach($articles as $article)
			<h3><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a><br /><small>{{ $article->published_at->diffForHumans() }}</small></h3>

			<blockquote>
				<p>{{ $article->subtitle }}</p>
				<footer>by <cite title="{{ $article->user->first_name }} {{ $article->user->last_name }}"><a href="{{ action('ArticleController@user', $article->user_id) }}">{{ $article->user->first_name }} {{ $article->user->last_name }}</a></cite></footer>
			</blockquote>

			<p>
				<a href="{{ action('PlaceController@articles', $article->place->id) }}">{{ $article->place->name }}</a> | 
				in <a href="{{ action('ChannelController@articles', $article->channel()->first()->id) }}">{{ $article->channel()->first()->name }}</a> |
				<i class="fa fa-thumbs-o-up"></i> 
				{{ count($article->likes) }}
			</p>

			@endforeach

			<div class="text-center">
				{{ $articles->appends(array('sort' => $sort, 'place' => $place))->links(); }}
			</div>
		</div>

		<div class="col-md-3">
		Advert
		</div>

	</div>
</div>

@stop

@section('scripts')
	@parent
	<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDI2I5xo_amHtzEVkYlp9ZZVSpGDu3FvbQ&sensor=true&libraries=places">
    </script>
    
    <script type="text/javascript">
    	$(function() {
    		initialize();

    		

		});

        function initialize() {

        	/*geocoder = new google.maps.Geocoder();

        	geocoder.geocode( { 'address': 'Griffin, QLD, Australia'}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
		        alert(results[0].geometry.location);
		        
		      } else {
		        alert("Geocode was not successful for the following reason: " + status);
		      }
		    });*/

			var input = document.getElementById('pac-input');

			var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.setTypes(['geocode']);

			var ldn = new google.maps.LatLng(51.50960920000001, -0.07610439999996288);

			var griffin = new google.maps.LatLng(-27.2636253, 153.03031839999994);
			var initialLocation;
			var browserSupportFlag =  new Boolean();

			var styles = [
			{
			  stylers: [
			    { hue: "#00ffe6" },
			    { saturation: -20 }
			  ]
			},{
			  featureType: "road",
			  elementType: "geometry",
			  stylers: [
			    { lightness: 100 },
			    { visibility: "simplified" }
			  ]
			}
			];

			var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

			var mapOptions = {
				zoom: 14,
				zoomControl: false,
				streetViewControl: false,
				panControl: false,
				mapTypeControl: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
				}
			};

			var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');

			if(navigator.geolocation) {
				browserSupportFlag = true;
				navigator.geolocation.getCurrentPosition(function(position) {
				//initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				map.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
			}, function() {
				handleNoGeolocation(browserSupportFlag);
				});
			}
			 // Browser doesn't support Geolocation
			else {
				browserSupportFlag = false;
				handleNoGeolocation(browserSupportFlag);
			}

			function handleNoGeolocation(errorFlag) {
			    if (errorFlag == true) {
			      initialLocation = ldn;
			    } else {
			      initialLocation = ldn;
			    }
			    map.setCenter(ldn);
			  }
        }
    </script>

@stop