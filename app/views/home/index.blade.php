@extends('layouts/default')

@section('content')
<header class="masthead">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 push-top">
				<div class="panel panel-default push-top">
					<div class="panel-body">
						<form role="form">
							<div class="form-group">
								<input type="text" class="form-control input-lg" id="auto-place" name="place" placeholder="Where are you?">
							</div>

							<button type="submit" class="btn btn-info btn-block">Search</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="gmap"></div>
</header>

<div class="container">
	<h1>Latest Articles<br /><small>{{ $place }}</small></h1>

	<ul class="nav nav-tabs">
		<li {{ ($sort == 'timeline') ? ' class="active"' : '' }}><a href="{{ action('HomeController@index') }}?sort=timeline&place={{ $place }}">Most Recent</a></li>
		<li {{ ($sort == 'popular') ? ' class="active"' : '' }}><a href="{{ action('HomeController@index') }}?sort=popular&place={{ $place }}">Most Popular</a></li>
	</ul>

	<hr />

	<div class="row" id="container">
	{{ Advert::insertInto($articles, 'partials/article_grid', 'partials/advert_grid') }}

	
	</div>

	<div class="text-center">
		{{ $articles->appends(array('sort' => $sort, 'place' => $place))->links(); }}
	</div>
	



</div>

@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
	
	<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDI2I5xo_amHtzEVkYlp9ZZVSpGDu3FvbQ&sensor=true&libraries=places">
    </script>
    
    <script type="text/javascript">
    	$(function() {
    		initialize();

    		var $container = $('#container');
			
			$container.masonry({
			  itemSelector: '.item'
			});

		});

        function initialize() {
			var autocomplete = new google.maps.places.Autocomplete( (document.getElementById('auto-place')), { types: ['geocode'] });
			
		
			var styles = [
			{
			  stylers: [
			    { hue: "#00ffe6" },
			    { saturation: -20 }
			  ]
			},{
			  featureType: "road",
			  elementType: "geometry",
			  stylers: [
			    { lightness: 100 },
			    { visibility: "simplified" }
			  ]
			}
			];

			var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

			var mapOptions = {
				zoom: 14,
				zoomControl: false,
				streetViewControl: false,
				panControl: false,
				mapTypeControl: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
				}
			};

			var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');
			map.setCenter( new google.maps.LatLng({{ $location_str }}) );

			
        }
    </script>

@stop