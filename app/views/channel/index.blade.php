@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1>Channels</h1>

	<ul class="nav nav-tabs">
		<li{{ ($sort == 'timeline') ? ' class="active"' : '' }}><a href="{{ action('ChannelController@index') }}?sort=timeline">Most Recent</a></li>
		<li{{ ($sort == 'popular') ? ' class="active"' : '' }}><a href="{{ action('ChannelController@index') }}?sort=popular">Most Popular</a></li>
	</ul>
	
	<hr />

	<div class="row" id="container">
		@foreach($channels->all() as $channel)
			@include('partials/channel_grid')
		@endforeach

	</div>

	<div class="text-center">
		{{ $channels->appends(array('sort' => $sort))->links(); }}
	</div>
</div>
@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

	<script type="text/javascript">
		<!--
		$(function() {

			var $container = $('#container');
			// initialize
			$container.masonry({
			  itemSelector: '.item'
			});

		});
		-->
	</script>

@stop