@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1 class="page-header">Edit Channel</h1>

	{{
    Form::model(
    			$channel,
    			array(
                    'action' => array('ChannelController@handleEdit', $channel->id),
                    'role'		=> 'form'
                    ))
    }}

		<div class="form-group">
			<label>Name</label>
	        {{ Form::text('name', Input::get('name'), array('class' => 'form-control')) }}
		</div>

		<div class="form-group">
			<label>Description</label>
			{{ Form::textarea('description', Input::get('description'), array('class' => 'form-control')) }}
		</div>
		

		<button type="submit" class="btn btn-default">Save Channel</button>
	</form>
</div>

@stop