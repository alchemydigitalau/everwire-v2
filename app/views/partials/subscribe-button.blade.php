@if(Auth::check())
	@if($subscribed = in_array($channel->id, $subscriptions))
		{{ Form::open(array( 'method' => 'DELETE', 'route' => array('channel.unsubscribe', $channel->id )) ) }}
	@else
	{{ Form::open(array( 'route' => 'channel.subscribe')) }}
		{{ Form::hidden('channel-id', $channel->id) }}
	@endif
		<button type="submit" class="btn {{ $subscribed ? 'btn-success' : 'btn-default' }}">
			<i class="fa fa-rss"></i> Subscribe{{ $subscribed ? 'd' : '' }}
		</button>
	{{ Form::close() }}
@endif