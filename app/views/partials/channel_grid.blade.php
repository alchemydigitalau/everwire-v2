<div class="col-sm-6 col-md-4 col-lg-3 item">
	<div class="panel panel-default">
		<div class="panel-heading image" style="background-image: url({{ $channel->preview('strip') }});">
			
		</div>

		<div class="panel-body">
			<h4><a href="{{ action('ChannelController@articles', $channel->id) }}">{{ $channel->name }}</a></h4>
			
			<p>{{ $channel->description }}</p>

			<p>
				<i class="fa fa-users"></i> {{ $channel->subscribers->count() }} | <i class="fa fa-file"></i> {{ $channel->articles->count() }}

				@include('partials/subscribe-button')

				@if(Auth::check() && $channel->user_id == Auth::user()->id)
					<a href="{{ action('ChannelController@edit', $channel->id) }}" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
				@endif
			</p>
		</div>
	</div>
</div>