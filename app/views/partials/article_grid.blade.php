<div class="col-sm-6 col-md-4 col-lg-3 item">
	<div class="panel panel-default">
		<div class="panel-heading image" style="background-image: url({{ $item->preview('strip') }});">
			<a href="{{ action('ArticleController@user', $item->user_id) }}" class="avatar">
				<img src="{{ $item->user->avatar() }}" class="img-circle img-thumbnail">
			</a>

		</div>


		<div class="panel-body">
			<h4>
				<a href="{{ action('ArticleController@show', $item->id) }}">{{ $item->title }}</a>
				@if( ! empty($item->published_at))
				<br /><time><small>{{ $item->published_at->diffForHumans() }}</small></time></p>
				@endif
			</h4>
			

			<blockquote>
				<p>{{ $item->subtitle }}</p>
				<footer>by <cite title="{{ $item->user->first_name }} {{ $item->user->last_name }}"><a href="{{ action('ArticleController@user', $item->user_id) }}">{{ $item->user->first_name }} {{ $item->user->last_name }}</a></cite></footer>
			</blockquote>

			
		</div>

		<div class="panel-footer">
			<p>
				<i class="fa fa-map-marker"></i> <a href="{{ action('PlaceController@articles', $item->place->id) }}">{{ $item->place->name }}</a> 
			</p>


				@if($item->channel()->count())
				<p>
					<i class="fa fa-folder-open"></i> <a href="{{ action('ChannelController@articles', $item->channel()->first()->id) }}">{{ $item->channel()->first()->name }}</a> 
				</p>
				@endif
				
				@if( ! empty($item->published_at))
				<p>
					<i class="fa fa-thumbs-o-up"></i> {{ count($item->likes) }}
				</p>
				
				@endif
			</div>
	</div>
</div>