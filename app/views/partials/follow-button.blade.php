@if(Auth::check() && Auth::user()->id != $user->id)
	@if($following = in_array($user->id, $followed))
		{{ Form::open(array( 'method' => 'DELETE', 'route' => array('user.unfollow', $user->id )) ) }}
	@else
	{{ Form::open(array( 'route' => 'user.follow')) }}
		{{ Form::hidden('user-id', $user->id) }}
	@endif
		<button type="submit" class="btn {{ $following ? 'btn-primary' : 'btn-default' }}">
			<i class="fa fa-thumbs-o-up"></i> Follow{{ $following ? 'ing' : '' }}
		</button>
	{{ Form::close() }}
@endif