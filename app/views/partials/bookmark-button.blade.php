@if(Auth::check())
	@if($bookmarked = in_array($article->id, $bookmarks))
		{{ Form::open(array( 'method' => 'DELETE', 'route' => array('article.unbookmark', $article->id )) ) }}
	@else
	{{ Form::open(array( 'route' => 'article.bookmark')) }}
		{{ Form::hidden('article-id', $article->id) }}
	@endif
		<button type="submit" class="btn {{ $bookmarked ? 'btn-danger' : 'btn-default' }}">
			<i class="fa fa-bookmark"></i> Bookmark{{ $bookmarked ? 'ed' : '' }}
		</button>
	{{ Form::close() }}
@endif