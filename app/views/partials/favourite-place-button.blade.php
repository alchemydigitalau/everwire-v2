@if(Auth::check())
	@if($favourited = in_array($place->id, $favouritePlaces))
		{{ Form::open(array( 'method' => 'DELETE', 'route' => array('place.unfavourite', $place->id )) ) }}
	@else
	{{ Form::open(array( 'route' => 'place.favourite')) }}
		{{ Form::hidden('place-id', $place->id) }}
	@endif
		<button type="submit" class="btn {{ $favourited ? 'btn-warning' : 'btn-default' }}">
			<i class="fa fa-map-marker"></i> Favourite{{ $favourited ? 'd' : '' }}
		</button>
	{{ Form::close() }}
@endif