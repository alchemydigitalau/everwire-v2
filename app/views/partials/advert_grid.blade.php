<div class="col-sm-6 col-md-4 col-lg-3 item">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Advert</h3>
		</div>
		
		<div class="panel-body">
		
			<h4>{{ $advert->title }}</h4>
			<blockquote>
				<p>{{ $advert->subtitle }}</p>
				<footer><a href="{{ $advert->link }}">{{ $advert->link }}</a></footer>
			</blockquote>

		</div>
	</div>
</div>