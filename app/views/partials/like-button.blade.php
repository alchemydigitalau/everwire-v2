@if(Auth::check())
	@if($liked = in_array($article->id, $likes))
		{{ Form::open(array( 'method' => 'DELETE', 'route' => array('article.unlike', $article->id )) ) }}
	@else
	{{ Form::open(array( 'route' => 'article.like')) }}
		{{ Form::hidden('article-id', $article->id) }}
	@endif
		<button type="submit" class="btn {{ $liked ? 'btn-primary' : 'btn-default' }}">
			<i class="fa fa-thumbs-o-up"></i> Like{{ $liked ? 'd' : '' }}
		</button>
	{{ Form::close() }}
@endif