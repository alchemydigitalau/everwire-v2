<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Everwire</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">everwire <small><span class="label label-info">Alpha</span></small></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ action('ChannelController@index') }}">Channels</a></li>
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        @if(Auth::check())
                        <li><a href="{{ action('BookmarkController@index') }}">Bookmarks</a></li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">New Article <b class="caret"></b></a>

                            <ul class="dropdown-menu">
                                <li><a href="{{ action('StoryController@create') }}">New Story</a></li>
                                <li><a href="#">New Gallery</a></li>
                                <li><a href="#">New Video</a></li>
                            </ul>
                        </li>
                        

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ action('ArticleController@user', Auth::user()->id ) }}">Your Articles</a></li>
                                <li><a href="{{ action('ChannelController@user' ) }}">Your Channels</a></li>
                                <li><a href="{{ action('AdvertController@user' ) }}">Your Adverts</a></li>
                                <li><a href="{{ action('UserController@settings' ) }}">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ action('AuthController@logout') }}">Logout</a></li>
                            </ul>
                        </li>
                        @else
                        <li><a href="{{ action('AuthController@login') }}">Login</a></li>
                        <li><a href="{{ action('AuthController@register') }}">Register</a></li>
                        @endif
                        
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


        @yield('content')


        <footer class="container">
            &copy; Everwire <?= date('Y') ?>
        </footer>

        @section('scripts')
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{ asset('js/jquery.js') }}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        @show
    </body>
</html>