@extends('layouts/default')

@section('content')
<header class="masthead" style="background-image: url({{ $article->preview('l')}});">
	<div class="container">	

		<div class="masthead-title">
			<h1>{{ $article->title }}<br />
				<small>
					@if( ! empty($article->published_at))
						<time title="{{ $article->published_at }}">{{ $article->published_at->diffForHumans() }}</time>
					@endif

					@if(Auth::check() && $article->user->id == Auth::user()->id)
					<a href="{{ action('ArticleController@edit', $article->id) }}" class="btn btn-default">
						<i class="fa fa-pencil"></i> Edit
					</a>
					@endif
				</small>
			</h1>

			<blockquote>
				<p>{{ $article->subtitle }}</p>
				<footer>
					by <cite title="{{ $article->user->first_name }} {{ $article->user->last_name }}">{{ $article->user->first_name }} {{ $article->user->last_name }}</cite>
					@include('partials/follow-button')
				</footer>
			</blockquote>
		</div>
	</div>
</header>

<div class="container">
	

	<div class="row">
	<div class="ol-sm-6 col-md-8 col-lg-9">

	{{ $article->payload->body_html }}

	@if( ! empty($article->published_at))
		@include('partials/like-button')

		@include('partials/bookmark-button')

		<a class="btn btn-default">
			<i class="fa fa-reply"></i> Rebound
		</a>

		<a class="btn btn-default">
			<i class="fa fa-flag"></i> Flag this article
		</a>
	@endif

	</div>

	{{ $advert }}

	</div>


	@if( ! empty($article->published_at))
	<div class="row">
		<div class="col-md-6">
			<h2 class="page-header"><i class="fa fa-user"></i> More articles by {{ $article->user->first_name }} {{ $article->user->last_name }}</h2>

			@foreach($by_user as $article)
			<h3><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a><br /><small>{{ $article->published_at->diffForHumans() }}</small></h3>

			<blockquote>
				<p>{{ $article->subtitle }}</p>
			</blockquote>

			<p>
				<a href="{{ action('PlaceController@articles', $article->place->id) }}">{{ $article->place->name }}</a> | 
				<i class="fa fa-thumbs-o-up"></i> 
				{{ count($article->likes) }}
			</p>

			@endforeach
		</div>

		<div class="col-md-6">
			<h2 class="page-header"><i class="fa fa-map-marker"></i> More articles in {{ $article->place->name }}</h2>

			@foreach($by_place as $article)
			<h3><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a><br /><small>{{ $article->published_at->diffForHumans() }}</small></h3>

			<blockquote>
				<p>{{ $article->subtitle }}</p>
				<footer>by <cite title="{{ $article->user->first_name }} {{ $article->user->last_name }}"><a href="{{ action('ArticleController@user', $article->user_id) }}">{{ $article->user->first_name }} {{ $article->user->last_name }}</a></cite></footer>
			</blockquote>

			<p>
				<i class="fa fa-thumbs-o-up"></i> 
				{{ count($article->likes) }}
			</p>

			@endforeach
		</div>


	</div>
	@endif
</div>

@stop