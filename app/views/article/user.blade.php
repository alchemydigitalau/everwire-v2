@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1>Articles by {{ $user->first_name }} {{ $user->last_name }}</h1>

	@include('partials/follow-button')

	<ul class="nav nav-tabs">
		<li{{ ($sort == 'timeline') ? ' class="active"' : '' }}><a href="{{ action('ArticleController@user', $user->id) }}?sort=timeline">Most Recent</a></li>
		<li{{ ($sort == 'popular') ? ' class="active"' : '' }}><a href="{{ action('ArticleController@user', $user->id) }}?sort=popular">Most Popular</a></li>
		@if(Auth::check() && $user->id == Auth::user()->id)
		<li{{ ($sort == 'draft') ? ' class="active"' : '' }}><a href="{{ action('ArticleController@drafts') }}">Draft Articles</a></li>
		@endif
	</ul>
	
	<hr />

	<div class="row" id="container">
		{{ Advert::insertInto($articles, 'partials/article_grid', 'partials/advert_grid') }}
	</div>

	<div class="text-center">
		{{ $articles->appends(array('sort' => $sort))->links(); }}
	</div>
</div>

@stop

@section('scripts')
	@parent
	<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

	<script type="text/javascript">
		<!--
		$(function() {

			var $container = $('#container');
			// initialize
			$container.masonry({
			  itemSelector: '.item'
			});

		});
		-->
	</script>

@stop