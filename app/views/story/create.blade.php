@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1 class="page-header">New Story</h1>

	{{
    Form::open(array(
                    'action' => 'StoryController@handleCreate',
                    'role'		=> 'form'
                    ))
    }}

		<div class="form-group">
			<label>Title</label>
	        {{ Form::text('title', Input::get('title'), array('class' => 'form-control')) }}
		</div>

		<div class="form-group">
			<label>Subtitle</label>
        	{{ Form::text('subtitle', Input::get('subtitle'), array('class' => 'form-control')) }}
		</div>

		<div class="form-group">
			<label>Body</label>
			{{ Form::textarea('body', Input::get('body'), array('class' => 'form-control', 'style'=> 'height: 300px;')) }}
		</div>

		<div class="form-group">
			<label>Place</label>
        	{{ Form::text('place', Input::get('place'), array('class' => 'form-control', 'id' => 'auto-place')) }}
		</div>
		

		<button type="submit" class="btn btn-default">Save Story</button>
		<input type="submit" class="btn btn-success" name="publish" value="Publish Now">
	</form>
</div>

@stop

@section('scripts')
	@parent
	<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDI2I5xo_amHtzEVkYlp9ZZVSpGDu3FvbQ&sensor=true&libraries=places">
    </script>
    
    <script type="text/javascript">
    	$(function() {
    		var autocomplete = new google.maps.places.Autocomplete( (document.getElementById('auto-place')), { types: ['geocode'] });
		});
    </script>

@stop