@extends('layouts/default')

@section('content')
<div class="container push-top">
	<h1>Login</h1>
	<form role="form" method="post">
		
		@include('partials/form_errors')

		<input type="text" class="form-control" name="email" placeholder="Email">

		<input type="password" class="form-control" name="password" placeholder="Password">

		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>
@stop