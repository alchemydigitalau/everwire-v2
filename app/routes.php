<?php

//Auth::loginUsingId(5);
//Auth::logout();

Route::model('article', 'Article');
Route::model('channel', 'Channel');
Route::model('place', 'Place');
Route::model('user', 'User');


Route::get('/', 'HomeController@index');

Route::group(array('prefix' => 'article'), function() {
	Route::get('/create/story', 'StoryController@create')->before('auth');
	Route::post('/create/story', 'StoryController@handleCreate')->before('auth|csrf');

	Route::get('/user/{user}', 'ArticleController@user');
	Route::get('/drafts', 'ArticleController@drafts')->before('auth');
	Route::get('/preview/{article}', 'ArticleController@preview')->before('auth');

	Route::get('/edit/{article}', 'ArticleController@edit');
	Route::get('/edit/story/{article}', 'StoryController@edit');
	Route::post('/edit/story/{article}', 'StoryController@handleEdit');

	Route::post('/like', 					array('as' => 'article.like', 'uses' => 'ArticleController@like') )->before('auth|csrf');
	Route::delete('/like/{article}', 		array('as' => 'article.unlike', 'uses' => 'ArticleController@unLike'))->before('auth|csrf');
	Route::post('/bookmark', 				array('as' => 'article.bookmark', 'uses' => 'ArticleController@bookmark') )->before('auth|csrf');
	Route::delete('/bookmark/{article}', 	array('as' => 'article.unbookmark', 'uses' => 'ArticleController@unBookmark'))->before('auth|csrf');

	Route::get('/{article}', 'ArticleController@show');
});

Route::group(array('prefix' => 'place'), function() {
	Route::post('/favourite', 				array('as' => 'place.favourite', 'uses' => 'PlaceController@favourite') )->before('auth|csrf');
	Route::delete('/favourite/{place}', 	array('as' => 'place.unfavourite', 'uses' => 'PlaceController@unFavourite'))->before('auth|csrf');

	Route::get('/{place}', 'PlaceController@articles');
});

Route::group(array('prefix' => 'channels'), function() {
	Route::get('/', 'ChannelController@index');
	Route::get('/user', 'ChannelController@user')->before('auth');
});

Route::group(array('prefix' => 'channel'), function() {
	

	Route::get('/create', 'ChannelController@create')->before('auth');
	Route::post('/create', 'ChannelController@handleCreate')->before('auth|csrf');

	Route::get('/edit/{channel}', 'ChannelController@edit')->before('auth');
	Route::post('/edit/{channel}', 'ChannelController@handleEdit')->before('auth|csrf');

	Route::post('/subscribe', 				array('as' => 'channel.subscribe', 'uses' => 'ChannelController@subscribe') )->before('auth|csrf');
	Route::delete('/subscribe/{channel}', 	array('as' => 'channel.unsubscribe', 'uses' => 'ChannelController@unSubscribe'))->before('auth|csrf');

	Route::get('/{channel}', 'ChannelController@articles');
});

Route::group(array('prefix' => 'adverts'), function() {
	Route::get('/user', 'AdvertController@user')->before('auth');
});

Route::group(array('prefix' => 'advert'), function() {
	Route::get('/create', 'AdvertController@create')->before('auth');
});

Route::group(array('prefix' => 'user'), function() {
	Route::get('/settings', 'UserController@settings');

	Route::post('/follow', 				array('as' => 'user.follow', 'uses' => 'UserController@follow') )->before('auth|csrf');
	Route::delete('/follow/{user}', 	array('as' => 'user.unfollow', 'uses' => 'UserController@unFollow'))->before('auth|csrf');

});

Route::group(array('prefix' => 'bookmarks'), function() {
	Route::get('', 'BookmarkController@index');
});

Route::group(array('prefix' => 'auth'), function() {
	Route::get('/register', 'AuthController@register');
	Route::post('/register', 'AuthController@handleRegister');
	Route::get('/login', 'AuthController@login');
	Route::post('/login', 'AuthController@handleLogin');
	Route::get('/logout', 'AuthController@logout');
});

// Serves up cached images if file doesn't already exist
Route::group(array('prefix' => 'img/cache'), function() {
	Route::get('/{args}', 'ImageController@cache')->where('args', '(.*)');
});


// Testing stuff
Route::get('follow', function()
{
	$u = Auth::user()->followers;

	foreach($u as $user)
	{
		echo $user->id;
	}
});

Route::get('artisan', function()
{
	//Artisan::call('db:seed');

	//exit;
	//$filesystem = new League\Flysystem\Filesystem( new League\Flysystem\Adapter\Local( base_path() . '/local_storage/places' ));

	$query = array(
                    'location'    => 'Leith, Scotland, United Kingdom',
                    'sensor'    => 'false',
                    'size'		=> '600x400',
                    'key'       => Config::get('geo.google_api_key')
                    );

    $image = Img::make( 'http://maps.googleapis.com/maps/api/streetview?' . http_build_query($query) );

    return $image->response();

    $image->encode('png');

    $filesystem->write( uniqid() . '.png', $image );

    //return $image->response();
	//Artisan::call('db:seed');
});

Route::get('/geo', function() {
	

    $place = Place::find_place('Fortitude Valley, Queensland, Australia');

    var_dump($place);
});